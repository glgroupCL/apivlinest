const request = require('request');

exports.ConsumeAPI = async function(direccion) {
	return new Promise((resolve, reject) => {

		var options = {
			url: `http://nominatim.openstreetmap.org/search/?format=json&q=${direccion}`,
			headers: {
				'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
				'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36'
			}
		};

		function callback(error, response, body) {
			var respuesta = {
				lat: null,
				lon: null
			}
			if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        if (Object.keys(info).length > 0) {
  				respuesta.lat = info[0].lat;
  				respuesta.lon = info[0].lon;
        }
			}
			resolve(respuesta);
		}
		request(options, callback);
	});

};
