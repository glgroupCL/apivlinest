let ubic = require('../config')
var ba64 = require('ba64')
let db = require('./sqlserver')
let moment = require('../node_modules/moment')
var ultimoNombre

exports.generarNombreImagen = async function (ImagenBase64, idManager) {

    let tecnico = { NumManager: parseInt(idManager) };
    var nombreTecnico = await db.query('dbconexion', 'spRec_Intranet_TraerPersonas', tecnico)
    console.log(nombreTecnico.recordset[0].Nombre_1)
    var nomTec = nombreTecnico.recordset[0].Nombre_1;
    var apeTec = nombreTecnico.recordset[0].Apellido_1;

    return new Promise((resolve, reject) => {
        try {

            console.log(nomTec + apeTec);
            var replace = ImagenBase64.includes("image/png") ? "data:image/png;base64," : "data:image/jpg;base64,";
            var extension = ImagenBase64.includes("image/png") ? ".png" : ".jpg";
            var fechaTexto = moment().format('YYYYMMDDHHmmssSSS');
            var random = (Math.random() * 100).toString().replace(".", "").substr(0, 3)
            var imgNombre = fechaTexto + "_" + random + "_" + nomTec + "_" + apeTec;

            resolve(imgNombre)

        } catch (error) {
            reject(error)
        }

    });
}
exports.generaUbicacion = async function (ImagenBase64, idManager, nombre) {
    return new Promise((resolve, reject) => {
        var imgNombre;
        try {
            var direccion = ubic.directorioImg;
            var imagenFinal = direccion + nombre;
            resolve(imagenFinal);
        } catch (error) {
            reject(error)
        }
    });
}

exports.guardarImagen = async function (imagenes, idManager, GuiaDeServicio) {
    imagenes.forEach(async (imagen) => {
        imagen.Imagen = imagen.Imagen.replace("image/jpeg;", "image/jpg;")
        var nombreImagen = await this.generarNombreImagen(imagen.Imagen, idManager)
        var directorio = await this.generaUbicacion(imagen.Imagen, idManager, nombreImagen)

        var spDataImagen = {}

        spDataImagen['numgs'] = GuiaDeServicio;
        spDataImagen['imgName'] = nombreImagen;
        spDataImagen['imagenBase64'] = imagen.Imagen;

        var vista = await db.query('dbconexion', 'spIns_ST_Imagenes', spDataImagen)
        ba64.writeImage(directorio, imagen.Imagen, function (err) {
            if (err) throw err;
        })
    });
}