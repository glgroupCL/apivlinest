// Inyección de dependencias
let express = require('express')
let bodyParser = require('body-parser')
let cors = require('cors')
const sql = require('mssql')
require('events').EventEmitter.defaultMaxListeners = Infinity;

// Inyección de archivos
//
//Configuración
let config = require('./config')

//Controllers Contador
let Servicio = require('./controllers/Solicitudes/ServicioController')

// Inicialización de la aplicación
var app = express()

// Confituración de nuestra API
app.use(bodyParser.json({limit: '100mb'}))
app.use(bodyParser.urlencoded({
  limit: '100mb',
  extended: true
}));
app.use(cors())
app.set('port', config.puerto)

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', config.domain)
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET')
  res.setHeader('Content-Type', 'application/json')
  next()
});

// Iniciamos las rutas de nuestro servidor/API
let rutas = express.Router()

// Ruta de bienvenida
rutas.get('/', function(req, res) {
  res.send({
    'Mensaje': 'Bienvenido a la API REST de ST desarrollada por el mejor equipo de TI'
  })
})


/* rutas.route('/auth/login')
  .post(Autentificacion.logear) */

//****** API 
//-------- c: rutas-------------------------------------

rutas.route('/ListadoUsuarios/:tkn')
.get(Servicio.ListadoUsuarios)

rutas.route('/ListadoGSTecnico/:tkn')
.get(Servicio.ListadoGSTecnico)

rutas.route('/ListadoSellos/:tkn')
.get(Servicio.ListadoSellos)

rutas.route('/ListadoMotivoCierre/:tkn')
.get(Servicio.ListadoMotivoCierre)

rutas.route('/ListadoMotivoInicioAtrasado/:tkn')
.get(Servicio.ListamotivoInicioAtrasado)

rutas.route('/ListadoEstadosSellos/:tkn')
.get(Servicio.ListaEstadosSellosExistentes)

rutas.route('/ListadoOpcionesCierreGS/:tkn')
.get(Servicio.ListaOpcionesCierre)

rutas.route('/InicioGS/:tkn')
.post(Servicio.InicioGS)

rutas.route('/EnviarGS/:tkn')
.post(Servicio.CierreGS)

rutas.route('/ImagenesGSBase64/:tkn')
.post(Servicio.ImagenesSTBase64)

rutas.route('/Ubicaciones/')
.get(Servicio.Ubicaciones)
//-------- f: rutas---------------

app.use(rutas)

async function conectar(){

  try {
    pool = await sql.connect(config["dbconexion"])
    console.log('Servidor de base de datos conectado');
  } catch (err) {
    console.log(`Error en la conexion servidor de base de datos, error ${err}`);
  }
}

// Inicialización del servicio
app.listen(config.puerto, function() {
  console.log(`Node server ejecutandose en http://localhost:${config.puerto}`)
  conectar();
})
