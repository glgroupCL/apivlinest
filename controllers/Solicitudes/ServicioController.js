var fs = require('fs')
var ba64 = require('ba64')
const nodemailer = require('nodemailer')
var moment = require('moment')

let provImagenes = require('../../providers/saveImage')
let config = require('../../config');
let db = require('../../providers/sqlserver');
let api = require('../../providers/http');
var correoTemplate = require('../../providers/correo')

var token = config.token;

//--------------inicio : MÉTODO DE LISTADO-----------------------------
//Lista usuarios
exports.ListadoUsuarios = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var vista = await db.query('dbconexion', 'spRec_ApiST_ListaUsuariosIntranetApi', {})
            res.status(200).json(vista.recordsets[0]);
        }
        else {
            console.log('token no válido');
        }
    }
    catch (exception) {
        console.log(exception);
    }
}

//Lista guías de servicio técnico
exports.ListadoGSTecnico = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {

            var final = []
            var vista = await db.query('dbconexion', 'spRec_ST_ListarGS', {})
            var arreglo = vista.recordsets[0]

            // promesas = [];
            // for (variable of arreglo) {
            //     promesas.push(api.ConsumeAPI(variable.Direccion));
            //     variable.lat = promesas.lat;
            //     variable.lng = promesas.lng;
            // }

            // final = await Promise.all(promesas);

            // for (var i = 0; i < arreglo.length; i++) {
            //     arreglo[i].Lat = final[i].lat;
            //     arreglo[i].Lng = final[i].lon;
            // }

            res.status(200).json(arreglo);
        }
        else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        console.log(exception);
    }
}

//Lista sellos
exports.ListadoSellos = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var vista = await db.query('dbconexion', 'spRec_ApiST_SellosTecnicos', {})
            res.status(200).json(vista.recordsets[0]);
        }
        else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        console.log(exception);
    }
}

//Lista los motivos de cierre
exports.ListadoMotivoCierre = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var vista = await db.query('dbconexion', 'spRec_ApiST_MotivoCierreAtrasado', {})
            res.status(200).json(vista.recordsets[0]);
        }
        else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        console.log(exception);
    }
}

//Lista los motivos de inicio atrasado
exports.ListamotivoInicioAtrasado = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var vista = await db.query('dbconexion', 'spRec_ApiST_MotivoInicioAtrasado', {})
            res.status(200).json(vista.recordsets[0]);
        }
        else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        console.log(exception);
    }
}

//Lista los estados
exports.ListaEstadosSellosExistentes = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var vista = await db.query('dbconexion', 'spRec_ApiST_EstadoSellos', {})
            res.status(200).json(vista.recordsets[0]);
        }
        else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        console.log(exception);
    }
}

//Lista las opciones de cierre
exports.ListaOpcionesCierre = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var vista = await db.query('dbconexion', 'spRec_ApiST_OpcionesCierre', {})
            res.status(200).json(vista.recordsets[0]);
        }
        else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        console.log(exception);
    }
}
//------------------fin MÉTODOS DE LISTADO---------------------------------------------------



//------------------inicio INICIO GUIA SERVICIO-----------------------------------------------------
exports.InicioGS = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var spData = {}
            spData['gs'] = req.body.GuiaDeServicio;
            spData['inicio'] = req.body.inicio;

            if (req.body.motivo != "") {
                spData['motivo'] = req.body.motivo;
            }
            var vista = await db.query('dbconexion', 'spIns_ApiST_IniciarGS', spData)
            res.status(200).json({
                ok: true
            });
        } else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        res.status(500).json({
            ok: false
        })
        console.log(exception);
    }
}
//------------------fin INICIO GUIA SERVICIO-----------------------------------------------------



//------------------inicio CIERRE GUIA SERVICIO--------------------------------------------------
exports.CierreGS = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            //Asignación de variables claves
            var _selloE = req.body.SellosExistentes;
            var _selloN = req.body.SellosNuevos;
            var imagenes = req.body.Imagenes;

            var RecepConforme = 0;

            if (req.body.RecepcionConforme == null ? true : false) {
                RecepConforme = 1;
            }

            var finit = req.body.FechaHoraInicio

            var spData = {
                numgs: req.body.GuiaDeServicio,
                nomcliente: req.body.PersonaRecepciona,
                email: req.body.CorreoRecepciona,
                desc: req.body.Comentarios,
                obs: req.body.MotivoCierre,
                conforme: RecepConforme,
                motivoAtraso: req.body.MotivoCierreAtrasado,
                comentarioTecnico: req.body.ComentarioTecnico,
                FechaHoraInicio: new Date(req.body.FechaHoraInicio),
				FechaCierre: new Date(req.body.FechaCierre)
            }
            //GUARDAR IMAGEN
            //Método que guarda las imagenes en directorio especificado en config
            provImagenes.guardarImagen(imagenes, req.body.IdManager, req.body.GuiaDeServicio);

            //ASIGNAR SELLOS
            //Asignación de sellos dañados
            _selloE.forEach(async (sello) => {
                let dataSE = {
                    sello: sello.NumeroSello,
                    gs: req.body.GuiaDeServicio,
                    estado: sello.Estado,
                    usuario: req.body.IdManager
                }
                var vista = await db.query('dbconexion', 'spMod_ST_ModificaEstadoDeSelloAanuladosYviolados', dataSE)
            });
            //Asignación de sellos nuevos
            _selloN.forEach(async (sello) => {
                let dataNE = {
                    desde: sello.Desde,
                    Hasta: sello.Hasta,
                    GS: req.body.GuiaDeServicio,
                    sucursal: req.body.Sucursal
                }
                var vista = await db.query('dbconexion', 'spMod_ST_InstalacionDeSellosEnCierre', dataNE)
            })

            try {
                //ENVÍO DE CORREO
                //Asignación de variables clave para envío de correo
                let nroGuia = await { NumGuiaServicio: parseInt(req.body.GuiaDeServicio) }
                let idMotivoCierre = await { IdMotivo: parseInt(req.body.MotivoCierre) }
                let nroCliente = await { Num: parseInt(req.body.IdManager) }

                //Retorno de variables clave solicitadas a sus correspondientes procedimientos almacenados
                let cliente = await db.query('dbconexion', 'spRec_Intranet_TraerCliente', nroCliente)
                let infoCorreo = await db.query('dbconexion', 'spRec_ST_GuiaDeServicio', nroGuia)
                let descripcionMotivo = await db.query('dbconexion', 'spRec_ST_nombreMotivo', idMotivoCierre)
                let fecha = moment(infoCorreo.recordset[0].FechaInicio)
                fecha = moment().format('DD-MM-YYYY')

                //Datos requeridos para el envío de correo
                let datosCorreo = {
                    NroDeGuíaDeServicio: req.body.GuiaDeServicio,
                    Cliente: infoCorreo.recordset[0].Contacto,
                    PersonaQueRecepciona: req.body.PersonaRecepciona,
                    Email: req.body.CorreoRecepciona,
                    FechaRealización: fecha,
                    Dirección: infoCorreo.recordset[0].Direccion,
                    NotaVentaAsociada: infoCorreo.recordset[0].NumNotaVenta,
                    TipoDeServicio: infoCorreo.recordset[0].TipoServicio,
                    TrabajoDirigidoPor: infoCorreo.recordset[0].NombreCompleto,
                    MotivoDeCierre: descripcionMotivo.recordset[0].Observacion,
                    DetalleCierre: req.body.Comentarios,
                    FirmaDeRecepcionante: imagenes[imagenes.length - 1],
                    Asunto: "Término de trabajo"
                }

                //Tabla contenedora de la información obtenida de las consultas para llenar el cuerpo.
                let tablaContenido = `
                <p>A continuación le indicamos el detalle de la guía de servicio recepcionada,</p>
                <table>
                    <tr>
                        <td>N° de Guía de servicio:</td>
                        <td>${datosCorreo.NroDeGuíaDeServicio}</td>
                    </tr>
                    <tr>
                        <td>Cliente:</td>
                        <td>${datosCorreo.Cliente}</td>
                    </tr>
                    <tr>
                        <td>Persona que recepciona:</td>
                        <td>${datosCorreo.PersonaQueRecepciona}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>${datosCorreo.Email}</td>
                    </tr>
                    <tr>
                        <td>Fecha de realización:</td>
                        <td>${datosCorreo.FechaRealización}</td>
                    </tr>
                    <tr>
                        <td>Dirección:</td>
                        <td>${datosCorreo.Dirección}</td>
                    </tr>
                    <tr>
                        <td>N° de nota de venta asociada:</td>
                        <td>${datosCorreo.NotaVentaAsociada}</td>
                    </tr>
                    <tr>
                        <td>Tipo de servicio:</td>
                        <td>${datosCorreo.TipoDeServicio}</td>
                    </tr>
                    <tr>
                        <td>Trabajo dirigido por:</td>
                        <td>${datosCorreo.TrabajoDirigidoPor}</td>
                    </tr>
                    <tr>
                        <td>Motivo de cierre:</td>
                        <td>${datosCorreo.MotivoDeCierre}</td>
                    </tr>
                    <tr>
                        <td>Detalle de cierre:</td>
                        <td>${datosCorreo.DetalleCierre}</td>
                    </tr>
                    <tr>
                        <td>Firma de recepcionante:</td>
                        <td>
                            <img src="${datosCorreo.FirmaDeRecepcionante.Imagen}" />
                        </td>
                    </tr>
                </table>`

                let cuerpo = await correoTemplate.template(datosCorreo.PersonaQueRecepciona, infoCorreo.Asunto, tablaContenido);

                //Métodos de envío de correo
                nodemailer.createTestAccount((err, account) => {
                    let transporter = nodemailer.createTransport({
                        host: config.host,
                        port: config.port,
                        secure: false,
                        ignoreTLS: true,
                        auth: {
                            user: config.user,
                            pass: config.pass
                        }
                    });

                    let mailOptions = {
                        from: config.user,
                        to: datosCorreo.Email,
                        cc: [config.conCopia, infoCorreo.recordset[0].CorreoVendedor],
                        bcc: config.conCopiaOculta,
                        subject: datosCorreo.Asunto,
                        html: cuerpo
                    };

                    transporter.sendMail(mailOptions, (error, info) => {
                        if (error) {
                            return console.log(error);
                        }
                        //console.log('Message sent: %s', info.messageId);
                        //console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

                    });
                });
            } catch (error) {
                console.log(error)
            }

            var vista = await db.query('dbconexion', 'spIns_ST_detalleCierreGS', spData)
            res.status(200).json({
                ok: true
            });
        } else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        res.status(500).json({
            ok: false
        })
        console.log(exception);
    }
}
//------------------fin CIERRE GUIA SERVICIO--------------------------------------------------
exports.ImagenesSTBase64 = async function (req, res) {
    try {
        var tkn = req.params.tkn;
        if (token == tkn) {
            var spData = {}
            spData['GuiaServicio'] = req.body.gs;

            db.query('dbconexion', 'spRec_ST_ObtieneImagenesBase64ST', spData).then(result => {
                res.status(200).json({ error: false, data: result.recordsets[0] });
            }).catch(err => {
                res.status(500).json({
                    error: true,
                    description: err
                })
            });

        } else {
            console.log('token no válido')
        }
    }
    catch (exception) {
        res.status(500).json({
            error: true,
            description: ''
        })
        console.log(exception);
    }
}

exports.Ubicaciones = async function (req, res) {

    var cont = 0;

    let result = await db.query('dbconexion', 'spRec_ST_guiaYDireccion', {});
        
    for (variable of result.recordsets[0]) {
        let resultado = await api.ConsumeAPI(variable.MAQ_VEHI);
        //console.log(resultado);
        //console.log(variable.NUMOT);
        var spData = {}
        spData['Latitud'] = resultado.lat;
        spData['Longitud'] = resultado.lon;
        spData['GuiaDeServicio'] = variable.NUMOT;
        cont += 1;
        //console.log(cont);
        await db.query('dbconexion', 'spMod_ST_ActualizaLatLongGuias', spData)
    }

    res.status(200).json({ ok: true });
}

